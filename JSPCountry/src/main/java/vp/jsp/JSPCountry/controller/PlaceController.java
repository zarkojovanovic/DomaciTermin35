package vp.jsp.JSPCountry.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vp.jsp.JSPCountry.model.Country;
import vp.jsp.JSPCountry.model.Place;
import vp.jsp.JSPCountry.service.CountryService;
import vp.jsp.JSPCountry.service.PlaceService;

@Controller
public class PlaceController {
	@Autowired
	PlaceService placeService;	
	
	@Autowired
	CountryService countryService;
	
	@RequestMapping(value="/places")
	public String getAllPlaces(Map<String, Object> model) {
		List<Place> places = placeService.findAll();
		
		model.put("places", places);
		
		return "places";
	}
	
	@RequestMapping(value= "/addPlacePage")
	public String redirectToAddPlacePage() {
		return "addPlace";
	}
	
	@RequestMapping(value = "/places/add", method = RequestMethod.POST)
	public String addPlace(HttpServletRequest request) {
		String zipCode = request.getParameter("zipCode");
		String name = request.getParameter("name");
		int countryId = Integer.parseInt(request.getParameter("countryId"));
		Country country = countryService.findOne(countryId);
		
		Place place = new Place(-1, Integer.valueOf(zipCode), name, country);
		placeService.save(place);
		
		return "redirect:/places";
		
	}
	
	@RequestMapping(value= "/places/remove", method=RequestMethod.GET)
	public String removePlace(HttpServletRequest request) {
		String id = request.getParameter("id");
		placeService.remove(Integer.valueOf(id));
		return "redirect:/places";
	}
	
	@RequestMapping(value="/modifyPlacesPage", method = RequestMethod.GET)
	public String goToModifyPlacePage(HttpServletRequest request, 
			Map<String, Object> model) {
		String id = request.getParameter("id");
		Place place = placeService.findOne(Integer.valueOf(id));
		
		model.put("place", place);
		
		return "modifyPlace";
	}
	
	@RequestMapping(value= "/places/modify", method= RequestMethod.POST)
	public String modifyPlace(HttpServletRequest request) {
		String id = request.getParameter("id");
		Place place = placeService.findOne(Integer.valueOf(id));
		place.setZipCode(Integer.valueOf(request.getParameter("zipCode")));
		place.setName(request.getParameter("name"));
		
		int countryId = Integer.valueOf(request.getParameter("countryId"));
		Country country = countryService.findOne(countryId);
		place.setCountry(country);
		
		
		placeService.save(place);
		
		return "redirect:/places";
		
	}
	
	@RequestMapping(value="/searchPlacePage")
	public String redirectToSearchPlacePage() {
		return "searchPlace";
	}
	
	@RequestMapping(value="/places/search")
	public String searchPlace(HttpServletRequest request) {
		if(request.getParameter("criteria").equals("id")) {
			return "searchPlaceById";
		} else if  (request.getParameter("criteria").equals("name")) {
			return "searchPlaceByName";
		
		} else if (request.getParameter("criteria").equals("county"));
			return "searchPlaceByCountry";
		
	}
	@RequestMapping(value="/places/search/name", method= RequestMethod.GET)
	public String searchPlaceByName(HttpServletRequest request, Map<String, Object> model) {
		String name = request.getParameter("name");
		List<Place> places = new ArrayList<Place>();
		places = placeService.findByName(name);
		model.put("places", places);
		return "places";
	}
	@RequestMapping(value="/places/search/id", method=RequestMethod.GET)
	public String searchPlaceById(HttpServletRequest request, Map<String, Object> model) {
		int id = Integer.valueOf(request.getParameter("id"));
		Place place = placeService.findOne(id);
		model.put("place", place);
		return "place";
	}
	@RequestMapping(value="/places/search/country", method=RequestMethod.GET)
	public String searchPlaceByCountry(HttpServletRequest request, Map<String, Object> model) {
		int countryId = Integer.valueOf(request.getParameter("countryId"));
		List<Place> places = new ArrayList<Place>();
		places = placeService.findByCountry(countryId);
		model.put("places", places);
		return "places";
	}
}
