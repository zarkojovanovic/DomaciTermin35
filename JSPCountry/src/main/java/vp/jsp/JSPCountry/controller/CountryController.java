 package vp.jsp.JSPCountry.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vp.jsp.JSPCountry.model.Country;
import vp.jsp.JSPCountry.service.CountryService;

@Controller
public class CountryController {
	@Autowired	
	CountryService countryService;
	
	@RequestMapping("/")
	public String welcome(Map<String, Object> model) {
		model.put("message", "Probna poruka");
		return "hellopage";
	}
	
	// metoda ce biti pozvana za url /api/countries/test
	@RequestMapping(value="/test") 
	public String test(Map<String, Object> model) {
		String s = "Hello!";
		model.put("message", s);
		return "hellopage"; // naziv jsp stranice koja treba biti prikazana nakon obrade zahteva
	}
	
	@RequestMapping(value="/countries") 
	public String getAllCountries(Map<String, Object> model) {
		List<Country> countries = countryService.findAll(); // preuzimanje liste drzava iz skladista
		
		model.put("countries", countries); // u request objekat ubacujemo objekat countries sa nazivom "countries"		
		
		return "countries"; // upucivanje na stranicu countries.jsp
	}
	
	@RequestMapping(value="/addCountryPage")
	public String redirectToAddCountryPage() {
		return "addCountry";
	}
	
	
	@RequestMapping(value="/countries/add", method=RequestMethod.POST)
	public String addCountry(HttpServletRequest request) {
		String name = request.getParameter("name");
		String population = request.getParameter("population");
		
		Country country = new Country(-1, name, Integer.parseInt(population)); // id ce biti postavljen pri snimanju
		countryService.save(country);
		
		// redirekcija na url /countries sto poziva metodu getAllCountries,
		// koja ce inicirati prikaz stranice countries.jsp
		return "redirect:/countries";  
	}
	
	@RequestMapping(value="/countries/remove", 
			method=RequestMethod.GET)
	public String removeCountry(HttpServletRequest request) {
		String id = request.getParameter("id");
		countryService.remove(Integer.valueOf(id));
		return "redirect:/countries"; 
	}
	
	@RequestMapping(value="/modifyCountryPage", 
			method=RequestMethod.GET)
	public String goToModifyCountryPage(HttpServletRequest request,
			Map<String, Object> model) {
		String id = request.getParameter("id");
		Country country = countryService.findOne(Integer.valueOf(id));
		
		model.put("country", country);
		
		return "modifyCountry"; // naziv jsp stranice na koju idemo 
	}
	
	@RequestMapping(value="/countries/modify", 
			method=RequestMethod.POST)
	public String modifyCountry(HttpServletRequest request) {
		int id = Integer.valueOf(request.getParameter("id"));
		String name = request.getParameter("name");
		int population = Integer.valueOf(request.getParameter("population"));
		
		Country country = new Country(id, name, population);
		countryService.save(country);
		
		return "redirect:/countries";		
	}
	@RequestMapping(value="/searchCountryPage")
	public String redirectToSearchCountryPage() {
		return "searchCountry";
	}
	@RequestMapping(value="/countries/search")
	public String searchCountry(HttpServletRequest request) {
		if(request.getParameter("criteria").equals("id")) {
			return "searchById";
		} else if (request.getParameter("criteria").equals("name")) {
			return "searchByName";
		}
		return null;
	}
	
	@RequestMapping(value="/countries/search/id", method=RequestMethod.GET)
	public String searchCountryById(HttpServletRequest request, Map<String, Object> model) {
		int id = Integer.valueOf(request.getParameter("id"));
		Country country = countryService.findOne(id);
		model.put("country", country);
		return "country";
	}
	
	@RequestMapping(value="/countries/search/name", method=RequestMethod.GET)
	public String searchCountryByName(HttpServletRequest request, Map<String, Object> model) {
		String name = request.getParameter("name");
		List<Country> countries = new ArrayList<Country>();
		countries = countryService.findByName(name);
		model.put("countries", countries);
		return "countries";
		
	}
}
