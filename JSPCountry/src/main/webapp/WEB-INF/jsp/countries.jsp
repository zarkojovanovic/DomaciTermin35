<!DOCTYPE html>
<%@ page import="vp.jsp.JSPCountry.model.Country" %>
<%@ page import="java.util.List" %>

<html>

	<body>
		<h1> Drzave </h1>
		
		<% List<Country> countries = (List<Country>) request.getAttribute("countries"); %>
		
		<table>
			<tr>
				<th>Naziv</th>
				<th>Populacija</th>
				<th>Akcije</th>
				<th></th>
			</tr>
			<% for (Country country: countries) { %> <!-- dinamicki dodamo onoliko redova u tabelu koliko ima drzava u listi -->
			
			<tr>
				<td> <%= country.getName() %> </td>
				<td> <%= country.getPopulation() %> </td>
				<td> <a href="/countries/remove?id=<%=country.getId() %>"> Obrisi </a> </td>
				<td> <a href="/modifyCountryPage?id=<%=country.getId() %>"> Izmeni  </a>  </td>
			</tr>
			
			<% } %>    <!-- kraj for petlje -->	
		</table>
		
		<br/>
		<a href="/addCountryPage"> Dodaj </a>
		<a href="/searchCountryPage"> Pretraga</a> 
	</body>
</html>