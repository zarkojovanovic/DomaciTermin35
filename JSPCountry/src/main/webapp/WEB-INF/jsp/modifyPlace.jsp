<%@ page import="vp.jsp.JSPCountry.model.Place" %>
<html>
	<body>
		<h1> Izmena grada </h1>
		
		<% Place place = (Place) request.getAttribute("place"); %>
		
		<form action="/places/modify" method="post">
			<input type="hidden" name="id" value="<%=place.getId() %>">
		
			<table>
				<tr>
					<td> Zip kod</td>
					<td> <input type="text" name="zipCode" value="<%= place.getZipCode()%>"></td>
				</tr>
				<tr>
					<td> Naziv </td>
					<td> <input type="text" name="name" value="<%= place.getName() %>"> </td>
				</tr>
				
				<tr>
					<td>Drzava</td>
					<td> <input type="text" name="countryId" value="<%= place.getCountry().getId() %>"> </td>
				</tr>
				
				<tr>
					<td colspan="2"> <input type="submit" value="Izmeni"> </td>
				</tr>
				
			</table>
			
			
		
		</form>
		
	</body>
</html>	