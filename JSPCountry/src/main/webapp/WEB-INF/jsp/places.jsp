<!DOCTYPE html>
<%@ page import="vp.jsp.JSPCountry.model.Place" %>
<%@ page import="java.util.List" %>

<html>

	<body>
		<h1> Gradovi </h1>
		
		<% List<Place> places = (List<Place>) request.getAttribute("places"); %>
		
		<table>
			<tr>
				<th>Zip kod</th>
				<th>Naziv</th>
				<th>Drzava</th>
				<th>Akcije</th>
				<th></th>
			</tr>
			<% for (Place place: places) { %> <!-- dinamicki dodamo onoliko redova u tabelu koliko ima drzava u listi -->
			
			<tr>
				<td> <%= place.getZipCode() %> </td>
				<td> <%= place.getName() %> </td>
				<th> <%= place.getCountry().getName() %></th>
				<td> <a href="/places/remove?id=<%=place.getId() %>"> Obrisi </a> </td>
				<td> <a href="/modifyPlacesPage?id=<%=place.getId() %>"> Izmeni  </a>  </td>
			</tr>
			
			<% } %>    <!-- kraj for petlje -->	
		</table>
		
		<br/>
		<a href="/addPlacePage"> Dodaj </a>
		<a href="/searchPlacePage"> Pretraga</a> 
	</body>
</html>